# Ratchet
## A templating development playground
Ratchet is a development playground for writing transforms using templating languages such as Velocity (Actually initially
it is designed to work with ONLY velocity).

## Quickstart
### 1. Run as Spring Boot project
```
$ git clone https://gitlab.com/thomasters/ratchet.git
$ cd ratchet
$ ./mvnw package
$ ./mvnw spring-boot:run
```
### 2. Run with Docker
```
$ docker pull thomasters/ratchet-docker:latest
$ docker run -p 8080:8080 -t thomasters/ratchet-docker
```

## About

### Architecture
Ratchet is developed as a Spring Boot 2.x application. It will be deployed in a docker container onto a suitable
platform.

### Technologies
* Maven - Wrapper set to Maven version 3.6.1
* Java 8
* Spring Boot 2.x
* Velocity - Refer to [Velocity](./documentation/velocity.md)
* Docker - Refer to [Docker](./documentation/docker.md)
* Jacoco - REfer to [Jacoco](./documentation/jacoco.md)

### How to guide

#### Freehand Mode
Free hand mode is the home page of the application. This page has 3 sections.
1. Language Mode - allows you to choose which templating language that you want to use. This changes to syntax highlighting
in the embedded IDE and also changes which functionality is used in the back end. This currently only has one language
which is Velocity but is open to accept more in the future if necessary.

2. Input/Output - This section consists of 2 embedded IDE's. The top one allows you to write Velocity code into it. The 
bottom on is read only. Enter some code into the top IDE, click the Run button and you will see the output on the IDE 
below. The user has access to various Velocity tools here. See more about these tools [Here](./documentation/velocity.md)

3. File Upload - This section allows you to upload a text file to be accessed as the body for the velocity template. The
content of the uploaded file can be accessed in the IDE under $body. Here is a basic use case for the file upload feature:

Say you were working with data in JSON format. you can upload that data (currently has to be in a .txt file) and access 
it using the necessary tools. E.G:
Uploaded File:
```
{"name":"John", "age":31, "city":"New York"}
```
Velocity Code:
```
#set ($jbody = $json.parse($body))
#set($name = $jbody.name)
#set($city = $jbody.city)
Hello my name is $name, i'm from $city.
```    
Output:
```
Hello my name is John, i'm from New York.
```
Here you can practise writing Velocity code and get instant access to the output.

#### Test Mode
Test mode is a similar set up to above. It is made up of 3 sections and is laid out like a unit test would be. This mode
allows you to run various variables through your macros and see if the expected output matches the actual output.
A typical user case of this would look something like the below:
Given:
```
#addNumbers(1,2)
```
When:
```
#macro( addNumbers $number1 $number2 )
$maths.add($number1, $number2)
#end
```
Then:
```
3
```
If the expected output matches that of the actual output then the box will turn green and you will get a message to let
you know that your test passes. 

##### Multi Tests
You are also able to test multiple input against multiple expected output. This can be used as follows:
Given:
```
{#addNumbers(1,2);
#addNumbers(1,9);
#addNumbers(7,4)}
```
When:
```
#macro( addNumbers $number1 $number2 )
$maths.add($number1, $number2)
#end
```
Then:
```
{3;
10;
11}
```
To use multi test mode you have to start the GIVEN and THEN blocks with a `{` and finish the block with a `}`. In-between
the two curly braces, you can provide a semi-colon separated list of value. This can either be done on multiple line (as shown
in the demo) or you can put them all on one line, whatever works for you! If your values include a semi-colon then you 
will need to escape it using a backslash.
E.G.
```
{hello /; world;}
```
Would output
```
{hello ; world;}
```