FROM openjdk:8-jre-alpine
ENV DEFAULT_JAVA_OPTS="-Duser.country=GB -Duser.language=en -Duser.timezone=Europe/London"
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
#TODO: an important improvement to the Dockerfile is to run the app as a non-root user: