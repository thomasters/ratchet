package uk.co.cdl.ratchet.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This controller determines which template to display to the user.
 */
@Controller
@RequestMapping
public class WelcomeController {

    /**
     * REQUEST url/
     * Will return index.html
     * @return - index.html
     */
    @RequestMapping("/")
    public String homePage() {
        return "index";
    }

    /**
     * REQUEST url/testmode
     * Will return testmode.html
     * @return - testmode.html
     */
    @RequestMapping("/testmode")
    public String testMode() {
        return "testmode";
    }

}