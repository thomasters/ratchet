package uk.co.cdl.ratchet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStoreService {

    private String path;

    @Autowired
    public FileStoreService(@Value("${upload.path}") String path){
        this.path = path;
    }

    public boolean uploadFile(MultipartFile file) {
        try {
            InputStream is = file.getInputStream();
            Files.copy(is, Paths.get(path + "input.txt"), StandardCopyOption.REPLACE_EXISTING);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

}