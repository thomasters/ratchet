package uk.co.cdl.ratchet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.co.cdl.ratchet.engineRoom.VelocityEngineRunner;

@RestController(value = "Ratchet API")
public class VelocityEngineController {

    private final VelocityEngineRunner velocityEngineRunner;

    @Autowired
    public VelocityEngineController(VelocityEngineRunner velocityEngineRunner){
        this.velocityEngineRunner = velocityEngineRunner;
    }

    @RequestMapping("transform")
    public ResponseEntity<String> getVelocityOutput(@RequestBody(required = false) String data){
        if(data == null || data.isEmpty()){
            return new ResponseEntity<>("", HttpStatus.OK);
        }
        return new ResponseEntity<>(velocityEngineRunner.runEngine(data), HttpStatus.OK);
    }
}
