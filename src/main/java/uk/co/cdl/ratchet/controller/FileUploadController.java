package uk.co.cdl.ratchet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import uk.co.cdl.ratchet.service.FileStoreService;

@Controller
public class FileUploadController {

    private final FileStoreService fileStoreService;

    @Autowired
    public FileUploadController(FileStoreService fileStoreService){
        this.fileStoreService = fileStoreService;
    }

    @RequestMapping(value = "/doUpload", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<String> upload(@RequestParam MultipartFile file) {
        if(unacceptedContentType(file.getContentType())){
            return new ResponseEntity<>("File had unaccepted content type", HttpStatus.BAD_REQUEST);
        }
        if(file.isEmpty()){
            return new ResponseEntity<>("File was empty.", HttpStatus.BAD_REQUEST);
        }
        if(!fileStoreService.uploadFile(file)){
            return new ResponseEntity<>("Error reading file.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private boolean unacceptedContentType(String contentType) {
        return contentType == null || !contentType.equalsIgnoreCase("text/plain");
    }
}
