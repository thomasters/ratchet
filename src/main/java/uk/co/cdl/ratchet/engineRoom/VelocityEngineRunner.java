package uk.co.cdl.ratchet.engineRoom;

import org.apache.commons.io.FileUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.apache.velocity.tools.generic.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@Service
public class VelocityEngineRunner {

    private String path;

    @Autowired
    public VelocityEngineRunner(@Value("${upload.path}") String path){
        this.path = path;
    }

    public String runEngine(String templateBody){
        VelocityEngine engine = new VelocityEngine();
        engine.init();

        VelocityContext variableMap = new VelocityContext();
        variableMap.put("body", getContentOf(path+"input.txt"));
        variableMap.put("date", new DateTool());
        variableMap.put("compareDate", new ComparisonDateTool());
        variableMap.put("json", new JsonTool());
        variableMap.put("maths", new MathTool());
        variableMap.put("xml", new XmlTool());
        variableMap.put("number", new NumberTool());
        variableMap.put("esc", new EscapeTool());
        variableMap.put("localDate", LocalDate.class);
        variableMap.put("dateTimeFormatter", DateTimeFormatter.class);
        variableMap.put("chronoUnit", ChronoUnit.class);

        StringWriter writer = new StringWriter();
        String result;
        try {
            engine.evaluate(variableMap, writer, "VelocityEngineRunner", templateBody);
            result = writer.toString();
        } catch (VelocityException ex){
            result = ex.getMessage();
        }
        return result;
    }

    private String getContentOf(String filename){
        final File file = new File(filename);
        try {
            return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

}
