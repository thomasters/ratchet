function executeTest() {
    const given = editor.getSession().getValue();
    const when = editorTwo.getSession().getValue();
    const then = $('#outputText').val();

    if (given.trim().startsWith("{")) {
        runMultiTest(given, when, then);
    } else {
        runTest(when + "\n" + given, then);
    }
}

function runTest(data, then) {
    $("#btn-execute").prop("disabled", true);

    transform(data)
        .done(function (data) {
            $('#testResultUpdate').html(getFeedback(data, 0, then));
        })
        .fail(function () {
            var feedback = "<div class=\"alert alert-warning\" role=\"alert\">Something went wrong</div>";
            $('#testResultUpdate').html(feedback);
        })
        .always(function () {
            $('#btn-execute').prop("disabled", false)
        });
}

function runMultiTest(given, when, then) {
    const givenArray = createStringArray(removeCurlyBraces(given));
    const thenArray = createStringArray(removeCurlyBraces(then));

    $("#btn-execute").prop("disabled", true);

    $('#testResultUpdate').html("");

    for (var i = 0; i < givenArray.length; i++) {
        transform(when + "\n" + givenArray[i])
            .done(function (data) {
                $('#testResultUpdate').append(getFeedback(data, i, thenArray[i]));
            })
            .fail(function () {
                var feedback = "<div class=\"alert alert-warning\" role=\"alert\">Something went wrong</div>";
                $('#testResultUpdate').html(feedback);
            })
            .always(function () {
                $('#btn-execute').prop("disabled", false)
            });
    }
}

function getFeedback(data, index, then) {
    if (data.toString().trim() === then.toString().trim()) {
        return "<div id=\"" + index + "\" class=\"alert alert-success\" role=\"alert\">Test passed:" +
            "<br>Expected output was: " + then + "<br>Actual output was: " + data + "</div>";
    } else {
        return "<div id=\"" + index + "\" class=\"alert alert-warning\" role=\"alert\">Test failed:" +
            "<br>Expected output was: " + then + "<br>Actual output was: " + data + "</div>";
    }
}

function createStringArray(textToConvert) {
    var strArray = textToConvert.split(/(?<!\/);(?![^(]*\))/);
    return strArray.map(x => x.replace('/;',';'));
}

// May need to put some checks in place here
function removeCurlyBraces(text) {
    return text.trim().slice(1, -1).trim();
}