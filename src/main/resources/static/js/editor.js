var editor = ace.edit("editor");
editor.setTheme("ace/theme/solarized_dark");
editor.getSession().setMode("ace/mode/velocity");

var editorTwo = ace.edit("editor-two");
editorTwo.setTheme("ace/theme/solarized_dark");
editorTwo.getSession().setMode("ace/mode/velocity");

function changeMode(value) {
    editor.getSession().setMode("ace/mode/" + value.toLowerCase());
}

function execute() {
    var editorText = editor.getSession().getValue();
    if (document.getElementById('mode').value === 'velocity') {
        runVelocity(editorText);
    }
}

function runVelocity(data) {
    $("#btn-execute").prop("disabled", true);

    transform(data)
        .done(function (data) {
            editorTwo.getSession().setValue(data);
        })
        .fail(function () {
            editorTwo.getSession().setValue("Oops Something Went Wrong!");
        })
        .always(function () {
            $('#btn-execute').prop("disabled", false)
        });
}

function setReadOnly(boolean){
    editorTwo.setReadOnly(boolean);
}