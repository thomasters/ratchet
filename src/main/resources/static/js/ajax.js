function transform(data) {
    return $.ajax({
        type: "POST",
        contentType: "text/plain",
        url: "/transform",
        data: data,
        cache: false,
        async: false
    })
}

function uploadFile(data) {
    return $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "doUpload",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000
    })
}