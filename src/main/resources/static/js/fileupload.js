$('#btnSubmit').click(function (event) {
    event.preventDefault();
    var form = $('#file-upload-form')[0];
    var data = new FormData(form);
    $("#btnSubmit").prop("disabled", true);

    uploadFile(data)
        .done(function () {
            var feedback = "<div class=\"alert alert-success\" role=\"alert\">Upload Success :)</div>";
            $('#uploadUpdate').html(feedback);
        })
        .fail(function (e) {
            var message = JSON.stringify(e.responseText);
            var feedback = "<div class=\"alert alert-warning\" role=\"alert\">Upload Failed! " + message + "</div>";
            $('#uploadUpdate').html(feedback);
        })
        .always(function () {
            $("#btnSubmit").prop("disabled", false);
        });
});

$(".custom-file-input").on("change", function () {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});