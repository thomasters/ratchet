package uk.co.cdl.ratchet.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class FileStoreServiceTest {

    private FileStoreService testSubject = new FileStoreService("src\\test\\resources\\upload\\");

    @Test
    public void shouldReturnTrueIfFileIsCorrectlyUploaded(){
        // Given
        MultipartFile file = new MockMultipartFile("file", "test.txt", "text/plain", "test text".getBytes());

        // When
        boolean result = testSubject.uploadFile(file);

        // Then
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseIfFileIsCorrectlyUploaded2() throws IOException {
        // Given
        MultipartFile file = Mockito.mock(MultipartFile.class);

        // When
        when(file.getInputStream()).thenThrow(IOException.class);
        boolean result = testSubject.uploadFile(file);

        // Then
        assertFalse(result);
    }

    //TODO: Test that checks that the correct text has been written to the correct file.

}