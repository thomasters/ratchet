package uk.co.cdl.ratchet.controller;

import io.restassured.RestAssured;
import io.restassured.builder.MultiPartSpecBuilder;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.io.File;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FileUploadControllerTest {

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    private File file = new File("src\\test\\resources\\testFile\\test.txt");

    @Test
    public void shouldReturnStatus200OKWhenFileIsSuccessfullyUploaded() {
        // Given
        given().port(port)
                .contentType("multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(file).fileName("file")
                        .mimeType("text/plain")
                        .build())
                .when().post("/doUpload")
                .then().statusCode(200);
    }

    @Test
    public void shouldReturnBadRequestIfFileContentTypeIsUnaccepted() {
        // Given
        given().port(port)
                .contentType("multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(file).fileName("file")
                        .mimeType("text/xml")
                        .build())
                .when().post("/doUpload")
                .then().statusCode(400).body(containsString("File had unaccepted content type"));
    }

    @Test
    public void shouldReturnBadRequestIfFileIsEmpty() {
        // Given
        File emptyFile = new File("src\\test\\resources\\testFile\\emptyFile.txt");
        given().port(port)
                .contentType("multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(emptyFile).fileName("file")
                        .mimeType("text/plain")
                        .build())
                .when().post("/doUpload")
                .then().statusCode(400).body(containsString("File was empty"));
    }

}