package uk.co.cdl.ratchet.controller;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class VelocityEngineControllerTest {

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    public void makeSureThatServiceIsUp() {
        given().port(port).get("/").then().statusCode(200);
    }

    @Test
    public void shouldNotThrowErrorWhenBodyIsEmpty(){
        given().port(port).body("")
                .when().post("/transform")
                .then().statusCode(200).contentType(ContentType.TEXT).body(containsString(""));
    }

    @Test
    public void shouldReturnGoodMorningFromTransform(){
        given().port(port).body("Good Morning!")
                .when().post("/transform")
                .then().statusCode(200).contentType(ContentType.TEXT).body(containsString("Good Morning!"));
    }

    @Test
    public void shouldReturnTransformedText(){
        given().port(port).body("#set($mood = \"Happy\")My mood is: $mood!")
                .when().post("/transform")
                .then().statusCode(200).contentType(ContentType.TEXT).body(containsString("My mood is: Happy!"));
    }

}