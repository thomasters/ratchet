package uk.co.cdl.ratchet.engineRoom;

import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class VelocityEngineRunnerTest {

    private String fileUploadPath = "src\\test\\resources\\upload\\";
    private VelocityEngineRunner testSubject = new VelocityEngineRunner(fileUploadPath);

    @Test
    public void shouldReturnTransformedText(){
        // Given
        String inputText = "Hello World!";

        // When
        String outputText = testSubject.runEngine(inputText);

        //Then
        assertThat(outputText, is("Hello World!"));
    }

    @Test
    public void shouldHaveAccessToDateTool(){
        // Given
        String inputText = "$date.toDate(\"dd MMMM yyyy\", \"17 January 1990\")";

        // When
        String outputText = testSubject.runEngine(inputText);

        //Then
        assertThat(outputText, is("Wed Jan 17 00:00:00 GMT 1990"));
    }

    @Test
    public void shouldHaveAccessToComparisonDateTool(){
        // Given
        String inputText = "$compareDate.toMinutes(\"1000000\")";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        // 1 million milliseconds is 16.6 minutes
        assertThat(outputText, is("16"));
    }

    @Test
    public void shouldHaveAccessToJSONTool(){
        // Given
        String inputText = "#set($jsonTest = $json.parse(\"[]\"))" + "$jsonTest";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("[]"));
    }

    @Test
    public void shouldHaveAccessToMathsTool(){
        // Given
        String inputText = "$maths.roundTo(\"2\", 2.36586563)";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("2.37"));
    }

    @Test
    public void shouldHaveAccessToXMLTool(){
        // Given
        String inputText = "#set($sampleXML = $xml.parse(\"<root>one</root>\"))$sampleXML";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("<root>one</root>"));
    }

    @Test
    public void shouldHaveAccessToNumberTool(){
        // Given
        String inputText = "$number.format('0', 2.656599)";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("3"));
    }

    @Test
    public void shouldHaveAccessToEscapeTool(){
        // Given
        String inputText = "$esc.xml(\"bread & butter\")";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("bread &amp; butter"));
    }

    @Test
    public void shouldHaveAccessToUploadedFileTextAsBody(){
        // Given
        String inputText = "$body";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("test text"));
    }

    @Test
    public void shouldHaveAccessToLocalDateTool(){
        // Given
        String inputText = "$localDate.of(2019, 11, 29)";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("2019-11-29"));
    }

    @Test
    public void shouldHaveAccessToDateTimeFormatterTool(){
        // Given
        String inputText = "$dateTimeFormatter.ofPattern(\"dd-MM-yyyy\")";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("Value(DayOfMonth,2)'-'Value(MonthOfYear,2)'-'Value(YearOfEra,4,19,EXCEEDS_PAD)"));
    }

    @Test
    public void shouldHaveAccessToChronoUnitTool(){
        // Given
        String inputText = "$chronoUnit.valueOf(\"YEARS\")";

        // When
        String outputText = testSubject.runEngine(inputText);

        // Then
        assertThat(outputText, is("Years"));
    }
}