context('Velocity Tools Tests', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080/')
    });

    it('transforms and displays text', function () {
        cy.get('#editor')
            .type('#macro(addNumbers $number1 $number2)$maths.add($number1, $number2)#end#addNumbers(1,2)');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', '3');
    });

    it('is able to access maths tool', function () {
        cy.get('#editor')
            .type('$maths.roundTo("2", "2.36586563")');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', '2.37');
    });

    it('is able to access date tool', function () {
        cy.get('#editor')
            .type('#set ($dateObj = $date.toDate("yyyy-MM-dd", "1990-01-17"))$date.getYear($dateObj)');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', '1990');
    });

    it('is able to access date comparison tool', function () {
        cy.get('#editor')
            .type('#set ($date1 = $date.toDate("yyyy-MM-dd", "2020-01-16"))' +
                '#set ($date2 = $date.toDate("yyyy-MM-dd", "2020-01-19"))' +
                '$compareDate.difference($date1, $date2).getDays()');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', '3');
    });

    it('is able to access escape tool', function () {
        cy.get('#editor')
            .type('#set($xml = $esc.xml("<note></note>"))$xml');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', '&lt;note&gt;&lt;/note&gt;');
    });

    it('is able to access number tool', function () {
        cy.get('#editor')
            .type('#set($myNumber = 13.55)$number.currency($myNumber)');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', '£13.55');
    });

    it('is able to access local date tool and chrono unit', function () {
        cy.get('#editor')
            .type('#set ($earlyDate = "17-01-1990")' +
                '#set ($lateDate = "18-01-1990")' +
                '#set($formatter = $dateTimeFormatter.ofPattern("dd-MM-yyyy"))' +
                '#set($dateOne = $localDate.parse($earlyDate, $formatter))' +
                '#set($dateTwo = $localDate.parse($lateDate, $formatter))' +
                'days: $chronoUnit.valueOf("DAYS").between($dateOne, $dateTwo)');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', 'days: 1');
    });

    it('is able to access date time formatter tool', function () {
        cy.get('#editor')
            .type('#set($formatter = $dateTimeFormatter.ofPattern("dd-MM-yyyy"))'+
                '#set($dateOne = $localDate.parse("17-01-1990", $formatter))'+
                '$dateOne');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', '1990-01-17');
    });
});
