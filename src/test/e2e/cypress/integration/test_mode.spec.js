context('Test Mode Tests', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080/testmode')
    });

    it('allows user to enter text into editor 1', function () {
        cy.get('#editor')
            .type('This is some text');
        cy.get('#editor').should('contain', 'This is some text');
    });

    it('allows user to enter text into editor 2', function () {
        cy.get('#editor-two')
            .type('This is some text');
        cy.get('#editor-two').should('contain', 'This is some text');
    });

    it('should display failed message if test fails', function () {
        cy.get('#editor')
            .type('#addNumbers(1,2)');
        cy.get('#editor-two')
            .type('#macro( addNumbers $number1 $number2 )' +
                '$maths.add($number1, $number2)' +
                '#end');
        cy.get('#outputText')
            .type('0');
        cy.get('#btn-execute').click();
        cy.get('#testResultUpdate').should('contain', 'Test failed:');
    });

    it('should display passed message if test passes', function () {
        cy.get('#editor')
            .type('#addNumbers(1,2)');
        cy.get('#editor-two')
            .type('#macro( addNumbers $number1 $number2 )' +
                '$maths.add($number1, $number2)' +
                '#end');
        cy.get('#outputText')
            .type('3');
        cy.get('#btn-execute').click();
        cy.get('#testResultUpdate').should('contain', 'Test passed:');
    });

    it('should display a separate result box for each test ran', function () {
        cy.get('#editor')
            .type('{#makeList("apple","banana","pear");' +
                '#makeList("peach","melon","pineapple");' +
                '#makeList("toffee","cookie","lolly");');
        cy.get('#editor-two')
            .type('#macro( makeList $item1 $item2 $item3 )'+
                'i want a $item1, $item2 and a $item3'+
                '#end');
        cy.get('#outputText')
            .type('{i want a apple, banana and a pear;' +
                'i want a peach, melon and a pineapple;' +
                'i want a toffee, cookie and a lolly;}',  {parseSpecialCharSequences: false});
        cy.get('#btn-execute').click();
        cy.get('#testResultUpdate').children().should('have.length', 3);
    });

    it('should display a separate result box for each test ran displaying the result of the test', function () {
        cy.get('#editor')
            .type('{#makeList("apple","banana","pear");' +
                '#makeList("peach","melon","pineapple");' +
                '#makeList("toffee","cookie","lolly");');
        cy.get('#editor-two')
            .type('#macro( makeList $item1 $item2 $item3 )'+
                'i want a $item1, $item2 and a $item3'+
                '#end');
        cy.get('#outputText')
            .type('{i want a apple, banana and a pear;' +
                'i want a peach, melon and a pineapple;' +
                'i want a toffee, cookie and a lolly;}',  {parseSpecialCharSequences: false});
        cy.get('#btn-execute').click();
        cy.get('#testResultUpdate').children().should('have.length', 3);
        cy.get('#0').should("contain", "Test passed:" +
            "Expected output was: i want a apple, banana and a pear" +
            "Actual output was: i want a apple, banana and a pear");
        cy.get('#1').should("contain", "Test passed:" +
            "Expected output was: i want a peach, melon and a pineapple" +
            "Actual output was: i want a peach, melon and a pineapple");
        cy.get('#2').should("contain", "Test passed:" +
            "Expected output was: i want a toffee, cookie and a lolly" +
            "Actual output was: i want a toffee, cookie and a lolly");
    });

});
