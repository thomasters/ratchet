context('Freehand Mode Tests', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080/')
    });

    it('allows user to enter text into editor 1', function () {
        cy.get('#editor')
            .type('This is some text');
        // Administrators see the users page by default
        cy.get('#editor').should('contain', 'This is some text');
    });

    it('does not allow user to enter text into editor 2', function () {
        cy.get('#editor-two')
            .type('This is some text');
        // Administrators see the users page by default
        cy.get('#editor-two').should('contain', '');
    });

    it('transforms and displays text', function () {
        cy.get('#editor')
            .type('#macro(addNumbers $number1 $number2)$maths.add($number1, $number2)#end#addNumbers(1,2)');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', '3');
    });

    it('allows user to upload a .txt file', function () {
        const fileName = 'data.txt';
        cy.fixture(fileName).then(fileContent => {
            cy.get('.custom-file-input').upload({ fileContent, fileName, mimeType: 'text/plain' });
        });
        cy.get('#btnSubmit').click();
        cy.get('#uploadUpdate').should('contain', 'Upload Success :)');
    });

    it('does not allow user to upload a .xml file', function () {
        const fileName = 'example.json';
        cy.fixture(fileName).then(fileContent => {
            cy.get('.custom-file-input').upload({ fileContent, fileName, mimeType: 'json' });
        });
        cy.get('#btnSubmit').click();
        cy.get('#uploadUpdate').should('contain', 'Upload Failed! "File had unaccepted content type"');
    });

    it('allows user to access data from uploaded .txt file in $body', function () {
        const fileName = 'data.txt';
        cy.fixture(fileName).then(fileContent => {
            cy.get('.custom-file-input').upload({ fileContent, fileName, mimeType: 'text/plain' });
        });
        cy.get('#btnSubmit').click();
        cy.get('#editor').type('$body');
        cy.get('#btn-execute').click();
        cy.get('#editor-two').should('contain', 'Uploaded txt file data.');
    });
});
