## TL;DR: 
### Pull and Run Docker
```
$ docker pull thomasters/ratchet-docker:latest
$ docker run -p 8080:8080 -t thomasters/ratchet-docker
```
### Run Local Docker
```
$ ./mvnw package && java -jar target/ratchet-docker-1.0-SNAPSHOT.jar
$ docker build -t thomasters/ratchet-docker .
$ docker run -p 8080:8080 -t thomasters/ratchet-docker
```

# Docker
This application has been containerised so that it is able to be ran in Docker.

## Docker File
Docker has a simple "Dockerfile" file format that it uses to specify the "layers" of an image. This is located in the
root of the project.

This Dockerfile is very simple, but that’s all you need to run a Spring Boot app. just Java and a JAR file. The build
will COPY the project JAR file into the container as "app.jar" that will be executed in the ENTRYPOINT.

For more information on this process [See this Docker guide](https://spring.io/guides/topicals/spring-boot-docker)

### Run using Docker
**Note** You will obviously need Docker installed to be able to do this. If you need to install docker you can do so by
visiting the [Docker website](https://docs.docker.com/docker-for-windows/install/).

#### Pull and Run Image From Docker Hub
This application has been pushed to the Docker Hub repository so you are able to simply pull the image and run it locally.
Open a shell and run the command `docker pull thomasters/ratchet-docker:latest`. This wil pull the latest version of the
docker image. You should now be able to see this when you run the command `docker image ls`. 

You can run this application by running the command `docker run -p 8080:8080 -t thomasters/ratchet-docker`. The
application should be running in a web browser at http://localhost:8080/ or by clicking [Here](http://localhost:8080/).

#### Build and Run Local Image
First of all you need to build the application using Maven so that it creates a jar in the target folder. Do this by
running the command `./mvnw package && java -jar target/ratchet-docker-1.0-SNAPSHOT.jar`

Next you need to build the docker image. Run `docker build -t thomasters/ratchet-docker .`

This command builds an image and tags it as thomasters/ratchet-docker.

Alternatively you can build the Dockerfile with maven like so: 
`./mvnw com.google.cloud.tools:jib-maven-plugin:dockerBuild -Dimage=thomasters/ratchet-docker`

At this point you have done everything that you need to do to run the application from your locally built docker file.
To do this, run the command: `docker run -p 8080:8080 -t thomasters/ratchet-docker`

This application should now be accessible in a web browser at http://localhost:8080/ or by clicking [Here](http://localhost:8080/).

Alternatively, you can push the docker image to Dockers image repository, Docker Hub. To see how to do this, read the
section 'Push to Docker Hub' below. Once you have completed this stage, you will be able to pull the docker image and
run the application that way. For more information on how to do this, please see the docker website for documentation.

#### Push to Docker Hub
If you wish to push the image to Docker Hub, you will need to have completed the steps above up to and including the
docker build command. Once your image has been successfully built locally use the following command to push the image:
`./mvnw com.google.cloud.tools:jib-maven-plugin:build -Dimage=thomasters/ratchet-docker`
**PLEASE NOTE**
To do this you will need to have permission to push to Dockerhub, which you won’t have by default. Change the image
prefix to your own Dockerhub ID, and docker login to make sure you are authenticated before you run Maven.
For more information, please see the docker login documentation [Here](https://docs.docker.com/engine/reference/commandline/login/)