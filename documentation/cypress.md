# Cypress Testing
## About 
Cypress is an end to end testing framework used in this application. For more information on Cypress, see the official 
documentation [Here](https://www.cypress.io/)

## Set-up
Before you are able to run Cypress tests locally you will need to install NodeJS and Cypress. For information on how to 
compleete the relevant set-up, please see the documentation [here](https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements)

## Run Cypress Tests
In order to run the cypress tests you need to fist need to start the application. Cypress assumes that the application 
will be running on localhost:8080 so if this isnt the case then this needs to be changed in the cypress.json file.

Tests can be ran in two way:
1. Open a terminal in src > test > e2e and run the command `npx cypress open`. This will open the main Cypress console
and you can select the tests that you want to run from there.

2. Open a terminal in src > test > e2e and run the command `npx cypress run --record --key db9b3f97-60b0-4107-8fb5-1049cb339adf --spec "cypress/integration/*"`.
This will run all of the test specs within cypress/integration folder and upload the video evidence to the account 
associated with the provided key.

## Video Evidence
Once you have ran the tests, reports and videos are saved to the relevant folder in the e2e > cypress folder structure.



