# Velocity
Ratchet supports velocity as a language. For documentation on how to write a velocity template, you can view the
Velocity documentation [here](https://velocity.apache.org/engine/devel/user-guide.html)

## Tooling
Ratchet provides some velocity tools which you can use to help perform some more complex transformations. Documentation
and usage examples are provided below.

##### JSON Tool
Documentation for this tool can be found [Here](https://velocity.apache.org/tools/devel/apidocs/org/apache/velocity/tools/generic/JsonTool.html)

To use this tool simply call the tool using $json.methodName(). In the following example, the json tool is being used to
parse the body of the data passed into the template into JSON and stores it in a variable called jbody.

`#set ($jbody = $json.parse($body))`

You can then call $jbody.keyName to get the value of the given key within the JSON map.

##### Maths Tool
Documentation for this tool can be found [Here](https://velocity.apache.org/tools/2.0/apidocs/org/apache/velocity/tools/generic/MathTool.html)

To use this tool simply call the tool using maths.methodName(). In the following example the maths tool is being used to
round a number to two decimal points.

`$maths.roundTo("2", "2.36586563")`

In this case, the template will print the value 2.37

##### Date Tool
Documentation for this tool can be found [Here](https://velocity.apache.org/tools/devel/apidocs/org/apache/velocity/tools/generic/DateTool.html)

To use this tool simply call the tool using date.methodName(). In the following example the date tool is being used to
convert a String to a Date object and then pull the year from it.

`#set ($dateObj = $date.toDate("yyyy-MM-dd'T'HH:mm:ss.SSSZ", $stringDate))
 $date.getYear($dateObj)`
 
##### Date Comparison Tool
Documentation for this tool can be found [Here](https://velocity.apache.org/tools/devel/apidocs/org/apache/velocity/tools/generic/ComparisonDateTool.html)

To use this tool simply call the tool using compareDate.methodName(). In the following example the compareDate tool is
being used to find the number of days between two dates.

`$compareDate.difference($aDateObj, $anotherDateObj).getDays()`

#### Xml Tool
Documentation for this tool can be found [Here](https://velocity.apache.org/tools/devel/apidocs/org/apache/velocity/tools/generic/XmlTool.html)

To use this tool simply call the tool using xml.methodName(). In the following example the xml tool is being used to
parse elements out of some xml.

    #if ($elementXml.name == "velocity")
        #set ($velocityXml = $xml.parse($lement))
        #set ($claimsValueMin = $velocityXml.claimsValueMin.Text)
        #set ($contactPostcode = $velocityXml.claimsValueMin.Text)

#### Escape Tool
Documentation for this tool can be found [Here](https://velocity.apache.org/tools/devel/apidocs/org/apache/velocity/tools/generic/EscapeTool.html)

To use this tool simply call the tool using esc.methodName(). In the following example the escape tool is being used to
escape output for xml string.

    #set ($xml = $body.toString())
    $esc.xml($xml)
    
#### Number Tool
Documentation for this tool can be found [Here](https://velocity.apache.org/tools/devel/apidocs/org/apache/velocity/tools/generic/NumberTool.html)

To use this tool simply call the tool using number.methodName(). Tool for working with Number in Velocity templates. It 
is useful for accessing and formatting arbitrary Number objects. Also the tool can be used to retrieve NumberFormat
instances or make conversions to and from various number types.

     $myNumber                   -gt; 13.55
     $number.format($myNumber)   -gt; 13.6
     $number.currency($myNumber) -gt; $13.55
     $number.integer($myNumber)  -gt; 13

#### Local Date Tool
Documentation for this tool can be found [Here](https://docs.oracle.com/javase/8/docs/api/java/time/LocalDate.html)

To use this tool simply call the tool using localDate.methodName(). In the following example the localDate tool is being
used to convert a string to a date Object using a given format.

    #set ($jbody = $json.parse($body))
    #set($formatter = $dateTimeFormatter.ofPattern("dd-MM-yyyy"))
    #set($dateOne = $localDate.parse($jbody.earlyDate, $formatter))
    #set($dateTwo = $localDate.parse($jbody.lateDate, $formatter))
    days: $chronoUnit.valueOf("DAYS").between($dateOne, $dateTwo)

#### Date Time Formatter Tool
Documentation for this tool can be found [Here](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html)

To use this tool simply call the tool using dateTimeFormatter.methodName(). Formatter for printing and parsing date-time objects.

    #set($formatter = $dateTimeFormatter.ofPattern("dd-MM-yyyy"))
    #set($dateOne = $localDate.parse($jbody.earlyDate, $formatter))
    
#### Chrono Unit Tool
Documentation for this tool can be found [Here](https://docs.oracle.com/javase/8/docs/api/java/time/temporal/ChronoUnit.html)

To use this tool simply call the tool using chronoUnit.methodName(). This tool provides a standard set of date periods units.

    days: $chronoUnit.valueOf("DAYS").between($dateOne, $dateTwo)
