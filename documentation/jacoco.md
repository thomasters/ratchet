## Jacoco - Java Code Coverage Report
This projects test coverage is analysed by Jacoco using the Jacoco maven plugin. This plugin is ran during the test
stage of the maven build and then the reports are published afterwards.

### How To Use
First run the command `./mvnw clean test`
Once the tests have ran, run the command `./mvnw jacoco:report`
Navigate to `ratchet > target > site > jacoco > index.html` and opn this file in your
browser of choice.

Here you will see the code coverage report for this project.

### Further Instrution
For more information on Jacoco, please find the documentation [here](https://www.jacoco.org/jacoco/trunk/doc/maven.html)